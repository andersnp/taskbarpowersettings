﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TaskbarPowerSettings.Services
{
    internal class PowerCfgService
    {
        private static void ActivateOverlay(string overlay)
        {
            var proc = Process.Start("powercfg.exe", $"/overlaysetactive {overlay}");
            proc.WaitForExit();
        }

        internal static void ActivatePowerSaver()
        {
            ActivateOverlay("OVERLAY_SCHEME_MIN");
        }

        internal static void ActivateBalanced()
        {
            ActivateOverlay("OVERLAY_SCHEME_NONE");
        }

        internal static void ActivateHighPerformance()
        {
            ActivateOverlay("OVERLAY_SCHEME_HIGH");
        }

        internal static void ActivateHighestPerformance()
        {
            ActivateOverlay("OVERLAY_SCHEME_MAX");
        }
    }
}
