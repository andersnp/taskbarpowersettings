﻿using Microsoft.UI.Xaml;
using Microsoft.UI.Xaml.Controls;
using Microsoft.UI.Xaml.Controls.Primitives;
using Microsoft.UI.Xaml.Data;
using Microsoft.UI.Xaml.Input;
using Microsoft.UI.Xaml.Media;
using Microsoft.UI.Xaml.Navigation;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.StartScreen;

// To learn more about WinUI, the WinUI project structure,
// and more about our project templates, see: http://aka.ms/winui-project-info.

namespace TaskbarPowerSettings.Views
{
    /// <summary>
    /// An empty window that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainWindow : Window
    {
        public MainWindow()
        {
            this.InitializeComponent();
        }

        private void myButton_Click(object sender, RoutedEventArgs e)
        {
            myButton.Content = "Clicked";
        }

        private async void Window_Activated(object sender, WindowActivatedEventArgs args)
        {
            var jl = await Windows.UI.StartScreen.JumpList.LoadCurrentAsync();
            if (jl.Items.Count != 4)
            {
                jl.SystemGroupKind = Windows.UI.StartScreen.JumpListSystemGroupKind.None;
                jl.Items.Clear();
                jl.Items.Add(JumpListItem.CreateWithArguments("--powersave", "Power Saving"));
                jl.Items.Add(JumpListItem.CreateWithArguments("--balanced", "Balanced"));
                jl.Items.Add(JumpListItem.CreateWithArguments("--high", "High Performance"));
                jl.Items.Add(JumpListItem.CreateWithArguments("--max", "Highest Performance"));
                await jl.SaveAsync();
            }
        }
    }
}
